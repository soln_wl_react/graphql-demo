import styled from 'styled-components';
import { Link } from 'react-router-dom';

const Body = styled.div`
    text-align: center;
`
const RouteLink = styled(Link)`
    text-decoration: none;
`
const Input = styled.input`
    height: 25px;
    margin: 6px;
    border-color: gray;
    padding-left: 8px;
    border-radius: 10px;
`
const SearchInput = Input.extend.attrs({
    type: 'text',
    value: null
})`
`
const SearchButton = Input.extend.attrs({
    type: 'button',
    value: 'Search',
    id: 'search'
})`
    cursor: pointer;
    width: 80px;
    height: 30px;
`

const SearchResultsDom = styled.div`
    color: blue;
    font-family: Arial;
    margin-top: 50px;
    box-sizing: border-box;
`
const ProductCount = styled.p`
    font-style: italic;
`
const ProductLists = styled.div`
    width: 100%
`
const Figure = styled.figure`
    height: auto;
    width: 200px;
    display: inline-block;
`
const Image = styled.img`
    height: 200px;
    width: 200px
`
const FigCaption = styled.figcaption`
    overflow: hidden;
    -webkit-line-clamp: 2;
    -webkit-box-orient: vertical;
    display: -webkit-box;
`
const BannerCont = styled.img`
    width: 85%;
    margin: 10px 30px;
`
const ProductDetailDom = SearchResultsDom
const ProductImgCont = styled.div`
    display: inline-block;
`
const ProductImage = styled.img.attrs({
    alt: 'product Image'
})`
    max-height: 350px
    max-width: 350px
`
const ProductData = styled.div`
    width: 40%;
    display: inline-block;
    text-align: left;
    vertical-align: top;
    margin-left: 50px;
`
const ProductName = styled.h3`
    
`

const ProductPrice = styled.div`
    font-size: 15px
    color: red;
    font-weight: bold;
`
const ProductDesc = styled.div`
    max-width: 80%
    margin-top: 20px
`
const QuantityCont = Input.extend.attrs({
    min: 1,
    max: 10,
    type: 'number'
})`
    margin: 10px 0px;
`
const AddToBagButton = Input.extend.attrs({
    type: 'button',
})`
    cursor: pointer;
    width: 110px
    height: 30px;
`
const PopUpModal = styled.div`
    left: 0;
    min-height: 100%;
    overflow: auto;
    position: absolute;
    top: 0;
    z-index: 1;
    bottom: 0;
    background-color: rgba(0,0,0,0.4);
    width: 100%;
`
const PopUpContent = styled.div`
    position: relative;
    margin: 0 auto 40px;
    margin-top: 40px;
    width: 750px;
    background-color: #fff;
    overflow: hidden;
`
const Heading = styled.h2`
    color: green;
    text-align: center;
    width: 93%;
    float: left;
`
const ProductDetCont = styled.div`
    overflow: hidden;
    margin: 0px 25px;
`
const DivElement = styled.div``
const ProductDetWrap = DivElement.extend`
    overflow: hidden;
    position: relative;
    margin: 10px 0px;
    border: 1px solid #000;
`
const ProductListImgCont = DivElement.extend`
    float: left;
    width: 25%;
`
const ProductCont = DivElement.extend`
    float: left;
    width: 75%;
`
const Pimage = styled.img`
    width: 130px;
    height: 130px;
    margin: 10px 12px;
`
const PName = DivElement.extend`
    font-size: 22px;
    color: blue;
    margin: 10px 0px;
    font-weight: bold;
`
const PQty = DivElement.extend`
    font-size: 15px;
    color: blue;
    margin: 8px 0px;
`
const PRemove = styled.button`
    font-size: 15px;
    margin: 8px 0;
    cursor: pointer;
`
const PPrice = DivElement.extend`
    font-size: 18px;
    color: blue;
    margin: 8px 0px;
`
const CartBtn = styled.button`
    width: 80px;
    cursor: pointer;
    margin: 6px;
    border-color: gray;
    padding-left: 8px;
    border-radius: 10px;
    height: 30px;
`
const PopUpCloseBtn = styled.img`
    width: 30px;
    height: 30px;
    margin-top: 19px;
    cursor: pointer;
`
const HeadCont = DivElement.extend`
    overflow:hidden;
`
const GifLoader = DivElement.extend`
    width: 100%; height: 100%; 
    background: url('/loading_spinner.gif') no-repeat center center
`
const GifComp = DivElement.extend`
    width: 250px;
    height: 250px;
    margin: 0 auto;
`
const AdminBtn = styled.img`
    width: 30px;
    height: 30px;
    margin-left: 15px;
    cursor: pointer;
    vertical-align: middle;
`
const AdminCartMainCont = DivElement.extend`
    font-size: 20px;
    color: blue;
    margin: 20px;
`
const AdminCartId =styled.button`
    margin: 6px;
    border-color: gray;
    padding-left: 8px;
    border-radius: 10px;
    cursor: pointer;
    width: 80px;
    height: 30px;
`
const CartP = styled.p`
    margin: 10px;
`
export {
    Body,
    RouteLink,
    SearchInput,
    SearchButton,
    SearchResultsDom,
    ProductCount,
    ProductLists,
    Figure,
    Image,
    FigCaption,
    BannerCont,
    ProductDetailDom,
    ProductImgCont,
    ProductImage,
    ProductData,
    ProductName,
    ProductPrice,
    ProductDesc,
    QuantityCont,
    AddToBagButton,
    PopUpModal,
    PopUpContent,
    Heading,
    ProductDetCont,
    ProductDetWrap,
    ProductListImgCont,
    ProductCont,
    Pimage,
    PName,
    PQty,
    PRemove,
    PPrice,
    CartBtn,
    PopUpCloseBtn,
    HeadCont,
    GifLoader,
    GifComp,
    AdminBtn,
    AdminCartMainCont,
    AdminCartId,
    CartP
};