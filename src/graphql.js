import ReactDOM from "react-dom";
import React, { Component } from "react";
import ApolloClient, { gql } from "apollo-boost";
import { ApolloProvider, Query, graphql } from "react-apollo";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import Cookies from 'universal-cookie';
import {  
  Body,
  RouteLink, 
  SearchInput,
  SearchButton, 
  SearchResultsDom, 
  ProductCount, 
  ProductLists, 
  Figure,
  Image,
  FigCaption,
  BannerCont,
  ProductDetailDom,
  ProductImgCont,
  ProductImage,
  ProductData,
  ProductName,
  ProductPrice,
  ProductDesc,
  QuantityCont,
  AddToBagButton,
  PopUpModal,
  PopUpContent,
  Heading,
  ProductDetWrap,
  ProductDetCont,
  ProductListImgCont,
  ProductCont,
  Pimage,
  PName,
  PQty,
  PRemove,
  PPrice,
  CartBtn,
  PopUpCloseBtn,
  HeadCont,
  GifLoader,
  GifComp,
  AdminBtn,
  AdminCartMainCont,
  AdminCartId,
  CartP
} from './styling';
import { observer } from "mobx-react";
import { observable } from "mobx";
import ReactGA from 'react-ga';

ReactGA.initialize('UA-1000001-1');
const modalRoot = document.getElementById('modal-root');

const cookies = new Cookies();
const client = new ApolloClient({
  uri: `//graphql-demo-api.herokuapp.com/graphql` //use localhost:4000 for local graphql server
});

const Search_Query = gql`
  query SearchQuery($searchTerm: String!) {
    search (searchTerm: $searchTerm, limit: 100, offset: 0) {
        productCount
        products {
          identifier
          image
          name
        } 
    }
  }
`;
const Product_Query = gql`
  query ProductQuery($productId: String!) {
    product (productId: $productId) {
      children {
        image
        identifier
        name
      }
      properties {
        description
        currencycode
        price
      } 
    }
  }
`;
const Cart_Query = gql`
  query CartQuery($cartId: String!) {
    getCart(cartId: $cartId) {
      cartId
      cartItems {
        productId
        Qty
        name
        image
        price 
      }
    }
  }
`;

const getAllCart = gql`
  query CartAllQuery {
    getCarts {
      cartId
      cartItems {
        productId
        Qty
        name
        image
        price 
      }
    }
  }
`

const addToBag_Mutation = gql`
  mutation updateCart ($cartId: String!, $productId: String!, $image: String!, $price: String!, $Qty: String!, $pdtName: String!) {
    addToCart(
      cartId: $cartId,
      productId: $productId,
      image: $image,
      price: $price,
      Qty: $Qty,
      pdtName: $pdtName
    ){
      cartId
      cartItems {
        productId
        Qty
        name
        image
        price
      }
    }
  }
`;
const removeItemFormCart = gql`
  mutation updateCart ($cartId: String!, $productId: String!) {
    removeFromCart(
      cartId: $cartId,
      productId: $productId
    )
    {
      cartId
      cartItems {
        productId
        Qty
        name
        image
        price
      }
    }
  }
`;

const Home = () => {
  return (
    <BannerCont src='/banner.png'/>
  );
};

const SearchResults = observer(
  class SearchResults extends Component
  { 
    render = () => (
      <Query query={Search_Query} variables={this.props.match.params} >
        {
          ({ loading, error, data, refetch, networkStatus }) => {
            if (loading) return (<GifComp><GifLoader/></GifComp>);
            if (error) return `Error!: ${error}`;

      return (
        <SearchResultsDom>
          <ProductCount>Product Count: {data.search.productCount}</ProductCount>
          <ProductLists>
            {data.search.products.map((prd) => {
              let prdUrl = '/product/' + prd.identifier;
              return (
              <Link to={prdUrl} title={prd.identifier}>
                <Figure>
                  <Image src={prd.image} />
                  <FigCaption> {prd.name} </FigCaption>
                </Figure>
              </Link>)
            })}
          </ProductLists>
        </SearchResultsDom>
      );
    }
    }
  </Query>
)});

class Modal extends React.Component {
  constructor(props) {
    super(props);
    this.el = document.createElement('div');
  }

  componentDidMount() {
    modalRoot.appendChild(this.el);
  }
  
  componentWillUnmount() {
    modalRoot.removeChild(this.el);
  }
  
  render() {
    return ReactDOM.createPortal(
      this.props.children,
      this.el,
    );
  }
}

const PriceSpan = {
  'fontSize': '20px',
  'fontWeight': 'bold',
  'color': 'red'
}

const renderProducts = (list, thisObj) => {
  var removeItems = async (target) => {
    const removeItemPostDate = {
      cartId: thisObj && thisObj.cartId || cookies.get('cartId'),
      productId: target.currentTarget.id
    }
    document.getElementById('Loading-wheel').style.display = 'block';
    await client.mutate({mutation: removeItemFormCart, variables: removeItemPostDate}).then(removeItemResponse => {
      if(removeItemResponse.data.removeFromCart && removeItemResponse.data.removeFromCart.cartItems && removeItemResponse.data.removeFromCart.cartItems.length)
        thisObj.state.cartDetails = removeItemResponse.data.removeFromCart.cartItems;
      else 
        thisObj.state.cartDetails = [{"productId":"TOYB2","Qty":"","name":"No Items in Cart","image":"//www.redbubble.com/assets/fe/sadOllied198187229d8d8a6785b1587f86182f5.gif","price":"","remove":"true"}];
    });
    document.getElementById('Loading-wheel').style.display = 'none';
  }

  return list.map((product, index) => {
    const { name, Qty, image , price, productId, remove} = product;
    return(
      <ProductDetWrap key={productId}>
        <ProductListImgCont>
          <Pimage src={image} />
        </ProductListImgCont>
        <ProductCont>
          <PName>{name}</PName>
          <PQty>Quantity: {Qty}</PQty>
          <PPrice>Total Price: <span style={PriceSpan}>${Math.round((Qty*price) * 100) / 100}</span></PPrice>
          <PRemove disabled={remove} id={productId} onClick={removeItems}>Remove Item</PRemove>
        </ProductCont>
      </ProductDetWrap>
    )
  });
};

const AddToBagContainer = observer(
class AddToBagContainer extends Component {
  state = observable({ selectVal: '1', value: 'Add', disabled: false, showModal: false, cartDetails: [], cartId: '' })
 
  handleQuantity = (e) => {
    this.state.selectVal = e.target.value
  }

  addProductToCart = async (productData, price) => {
    const addToBagPostData = {
      "cartId": this.state.cartId,
      "productId": productData.identifier,
      "image": productData.image,
      "price": price,
      "Qty": this.state.selectVal,
      "pdtName": productData.name
    };
    document.getElementById('Loading-wheel').style.display = 'block';
    await client.mutate({mutation: addToBag_Mutation, variables: addToBagPostData}).then(addToBagResponse => {
      this.state.cartDetails = addToBagResponse.data.addToCart.cartItems;
      this.state['showModal'] = true;
    });
    document.getElementById('Loading-wheel').style.display = 'none';
  }

  Addtobag = () => {
    this.state.cartId = cookies.get('cartId');
    if(this.state.value == 'Add') {
      this.state["value"] = 'Added';
      this.state["disabled"] = true;
      this.addProductToCart(this.props.productData, this.props.productProperties.price);
    }
  }

  handleHide = () => {
    this.state['showModal'] = false;
  }

  render() {
    let btnText = this.state.value + ' To Bag';
    const modal = this.state.showModal ? (
      <Modal>
      <PopUpModal>
        <PopUpContent>
          <HeadCont>
            <Heading>Shopping Cart</Heading>
            <PopUpCloseBtn onClick={this.handleHide} src='/closeImg.png'/>
          </HeadCont>
          <ProductDetCont>
            {renderProducts(this.state.cartDetails, this)}
          </ProductDetCont>
        </PopUpContent>
      </PopUpModal>
    </Modal>
    ) : null;

    return(
      <React.Fragment>
        <QuantityCont  value={this.state.selectVal} onChange={this.handleQuantity}/>
        <AddToBagButton onClick={this.Addtobag} value={btnText} disabled={this.state.disabled}/>
        {modal}
      </React.Fragment>
    )
  }
});

const ProductDetail = observer(
  class ProductDetail extends Component
  {
    render = () => (
      <Query query={Product_Query} variables={this.props.match.params} >
      {
        ({ loading, error, data, refetch, networkStatus }) => {
          if (loading) return (<GifComp><GifLoader/></GifComp>);
          if (error) return `Error!: ${error}`;
          ReactGA.event({ category: 'Product', action: 'Product Page', label: data.product.children.name})
          
          return (  
            <ProductDetailDom>        
              <ProductImgCont> 
                <ProductImage src={data.product.children.image}/>
              </ProductImgCont>
              <ProductData>
                <ProductCount>Product ID: {data.product.children.identifier}</ProductCount>           
                <ProductName>{data.product.children.name}</ProductName>
                <ProductPrice>Sale: {data.product.properties.currencycode}{data.product.properties.price}</ProductPrice>
                <AddToBagContainer productData = {data.product.children} productProperties = {data.product.properties}/>
                <ProductDesc>{data.product.properties.description}</ProductDesc>
              </ProductData>
            </ProductDetailDom>
          );
        }
      }   
      </Query>
    )
});

const cartAdmin = observer(
class cartAdmin extends Component {
  constructor(props) {
    super(props);
    this.cartId = null;
  }
  
  state = observable({data: [], showModal: false, cartDetails: [], cartId: ''})

  async componentDidMount() {
    await client.query({query: getAllCart, 'fetchPolicy': 'no-cache'}).then(response => {
      this.state.data = response.data.getCarts
    });

  }
  showCorspondingCart = async (target) => {
    this.cartId = target.currentTarget.id;
    await client.query({query: Cart_Query, variables: {cartId: target.currentTarget.id}, fetchPolicy: 'no-cache'}).then(cartResponse => {
      this.state.showModal = true;
      if(cartResponse.data.getCart && cartResponse.data.getCart.cartItems && cartResponse.data.getCart.cartItems.length) {
        this.state.cartDetails = cartResponse.data.getCart.cartItems;
      } else {
        this.state.cartDetails = [{"productId":"TOYB2","Qty":"","name":"No Items in Cart","image":"//www.redbubble.com/assets/fe/sadOllied198187229d8d8a6785b1587f86182f5.gif","price":"","remove":"true"}]
      }
    });
  }

  renderAllCarts = () => {
    const cartData = this.state.data
    return cartData.map((product, index) => {
      return (<CartP><AdminCartId onClick={this.showCorspondingCart} id={product.cartId} >{product.cartId}</AdminCartId></CartP>)
    })  
  };

  handleHide = () => {
    this.state.showModal = false;
  }
  
  render() {
    const renderAllCart = this.renderAllCarts()
    const modal = this.state.showModal ? (
      <Modal>
      <PopUpModal> 
        <PopUpContent>
          <HeadCont>
            <Heading>Shopping Cart</Heading>
            <PopUpCloseBtn onClick={this.handleHide} src='/closeImg.png'/>
          </HeadCont>
          <ProductDetCont>
            {renderProducts(this.state.cartDetails, this)}
          </ProductDetCont>
        </PopUpContent> 
      </PopUpModal>
    </Modal>
    ) : null;

    return (
      <AdminCartMainCont> List of CartId's
        {renderAllCart}
        {modal}
      </AdminCartMainCont>
    )
  }
});

const App = observer(
class App extends Component {
    state = observable({ searchTerm: null, catalogId: null, productId: null, searchUrl: '#', showModal: false, cartDetails: [] });
    constructor(props) {
      super(props);
      ReactGA.pageview(window.location.origin);
      //this.inputchange = this.inputchange.bind(this);
    }
    
    componentDidMount() {
      if (!cookies.get('cartId')) {
        let date = new Date() 
        date.setTime(date.getTime() + (30*24*60*60*1000))
        cookies.set('cartId', Math.round(10000+(Math.random()*(99999-10000))), { path: '/', expires: date})
      }
    }

    onSearch = async ({ target }) => {
      if (this.state.searchTerm)
        ReactGA.event({ category: 'Search', action: 'Catalog Search', label: this.state.searchTerm });
    }

    inputchange = event => {
        this.state.searchTerm = event.target.value;        
        let searchUrl = (this.state.searchTerm != '') ? ('/search/' + this.state.searchTerm) : '#';
        this.state.searchUrl = searchUrl;
        if(event.key == 'Enter')
          document.getElementById('search').click()
    }
    
    showCartModel = async () => {
      const currentCartId = cookies.get('cartId');
      document.getElementById('Loading-wheel').style.display = 'block';
      await client.query({query: Cart_Query, variables: {cartId: currentCartId},  'fetchPolicy': 'no-cache'}).then(cartResponse => {
        if(cartResponse.data.getCart && cartResponse.data.getCart.cartItems && cartResponse.data.getCart.cartItems.length) {
          this.state.cartDetails = cartResponse.data.getCart.cartItems;
        } else {
          this.state.cartDetails = [{"productId":"TOYB2","Qty":"","name":"No Items in Cart","image":"//www.redbubble.com/assets/fe/sadOllied198187229d8d8a6785b1587f86182f5.gif","price":"","remove":"true"}]
        }
        this.state.showModal = true;
      });
      document.getElementById('Loading-wheel').style.display = 'none';   
    }

    handleHide = () => {
      this.state.showModal = false;
    }

    render () {
      let response = {"data":{"addToCart":{"cartId":"65637","cartItems":[{"productId":"TOYB2","Qty":"1","name":"Liberty Imports World Racing Car Take-A-Part Toy for Kids with 30 Take Apart Pieces, Tool Drill, Lights and Sounds","image":"https://images-na.ssl-images-amazon.com/images/I/81V7c06o4JL._SX425_.jpg","price":"18.15","__typename":"cartItem"},{"productId":"moredesishoes","Qty":"1","name":"Baskets Galaxy Shoes","image":"https://d16rliti0tklvn.cloudfront.net/2495/1518721608003.1051255764.jpeg","price":"79.95","__typename":"cartItem"},{"productId":"toyboy15","Qty":"10","name":"Paw Patrol - Rescue Racers 3pk Vehicle Set Marshal Rubble, Rocky","image":"http://d16rliti0tklvn.cloudfront.net/2495/1476941710516.1646020217.jpg","price":"12.00","__typename":"cartItem"},{"productId":"toyboy13","Qty":"1","name":"Marvel Spider-Man Titan Hero Series Spider-Man 12-Inch Figure","image":"http://d16rliti0tklvn.cloudfront.net/2495/1476862620781.12045238.jpg","price":"10.00","__typename":"cartItem"},{"productId":"toyboy14","Qty":"4","name":"Marvel Titan Hero Series 3-Pack","image":"http://d16rliti0tklvn.cloudfront.net/2495/1476863686636.2054217757.jpg","price":"16.00","__typename":"cartItem"},{"productId":"TOYB1","Qty":"1","name":"WL Toys V757 Bubble Master Co-Axial 3.5 Channel RC Helicopter (Blue )","image":"https://images-na.ssl-images-amazon.com/images/I/51x4VwJBB7L._SX425_.jpg","price":"23.95","__typename":"cartItem"},{"productId":"SHOK1","Qty":"3","name":"Stride Rite Rugged Ritchie 2 Boot (Toddler/Little Kid)","image":"https://images-na.ssl-images-amazon.com/images/I/71zwXPfYk6L._UX500_.jpg","price":"39.78","__typename":"cartItem"},{"productId":"toygirl13","Qty":"2","name":"Disguise Disney's Frozen Elsa Deluxe Girl's Costume, 4-6X","image":"http://d16rliti0tklvn.cloudfront.net/2495/1476947092259.644940450.jpg","price":"18.00","__typename":"cartItem"}],"__typename":"cartData"}}};
      const modal = this.state.showModal ? ( 
        <Modal>
        <PopUpModal> 
          <PopUpContent>
            <HeadCont>
              <Heading>Shopping Cart</Heading>
              <PopUpCloseBtn onClick={this.handleHide} src='/closeImg.png'/>
            </HeadCont>
            <ProductDetCont>
              {renderProducts(this.state.cartDetails, this)}
            </ProductDetCont>
          </PopUpContent> 
        </PopUpModal>
      </Modal>
      ) : null;

      const adminLoginBtn = (cookies.get('cartId') == '13568') ? (
        <RouteLink to="/cartadmin"><AdminBtn src='/adminCartLogo.png' onClick={this.redirectToAdmin}/></RouteLink>
      ): null;

      return (
        <ApolloProvider client={client}>
          <Router>
            <Body>
              <RouteLink to="/" title="Demo App"><h1>Demo App for React & GraphQL</h1></RouteLink>
              <SearchInput onKeyUp={this.inputchange}/>
              <Link to={this.state.searchUrl} title={this.state.searchUrl} >
                <SearchButton onClick={this.onSearch} />
              </Link>
              <CartBtn onClick={this.showCartModel}>My Cart</CartBtn>
              {adminLoginBtn}
              {modal}

              <Route exact path="/" component={Home} />
              <Route path="/search/:searchTerm" component={SearchResults} />
              <Route path="/product/:productId" component={ProductDetail} />
              <Route path="/cartadmin" component={cartAdmin} />           
                        
            </Body>         
          </Router>           
        </ApolloProvider>
      )
    }
  }
);

export default App;
export { ProductDetail, SearchResults };